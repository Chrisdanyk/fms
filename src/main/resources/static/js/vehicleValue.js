/**
 *
 */

$('document').ready(function() {

    $('.table #editButton').on('click',function(event){
        event.preventDefault();
        var href= $(this).attr('href');
        $.get(href, function(vehicleValue, status){
            $('#idEdit').val(vehicleValue.id);
            $('#descriptionEdit').val(vehicleValue.description);
            $('#detailsEdit').val(vehicleValue.details);
        });
        $('#editModal').modal();
    });

    $('.table #detailsButton').on('click',function(event) {
        event.preventDefault();
        var href= $(this).attr('href');
        $.get(href, function(vehicleValue, status){
            $('#idDetails').val(vehicleValue.id);
            $('#descriptionDetails').val(vehicleValue.description);
            $('#detailsDetails').val(vehicleValue.details);
            $('#lastModifiedByDetails').val(vehicleValue.lastModifiedBy);
            $('#lastModifiedDateDetails').val(vehicleValue.lastModifiedDate.substr(0,19).replace("T", " "));
        });
        $('#detailsModal').modal();
    });

    $('.table #deleteButton').on('click',function(event) {
        event.preventDefault();
        var href = $(this).attr('href');
        $('#deleteModal #delRef').attr('href', href);
        $('#deleteModal').modal();
    });
});