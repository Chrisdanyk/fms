package cd.home.fms.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ApplicationController {
    @GetMapping({"/","/index"})
    public String application(){return "index";}

    @GetMapping("/layout")
    public String layout(){return "layouts/layout";}
}
