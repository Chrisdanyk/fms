package cd.home.fms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cd.home.fms.models.State;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {

}
