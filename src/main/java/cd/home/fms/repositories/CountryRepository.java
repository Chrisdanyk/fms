package cd.home.fms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cd.home.fms.models.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {

}
