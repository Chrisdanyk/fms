package cd.home.fms.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cd.home.fms.models.Location;

@Repository
public interface LocationRepository extends CrudRepository<Location, Integer> {

}
